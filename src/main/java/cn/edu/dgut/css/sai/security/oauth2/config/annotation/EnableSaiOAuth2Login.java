package cn.edu.dgut.css.sai.security.oauth2.config.annotation;

import cn.edu.dgut.css.sai.security.oauth2.config.SaiOAuth2ClientRegistrationRepositoryConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SaiOAuth2ClientRegistrationRepositoryConfiguration.class)
public @interface EnableSaiOAuth2Login {

}
